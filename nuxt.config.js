import colors from 'vuetify/es5/util/colors'
import dotenv from 'dotenv'
const env = dotenv.config()

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: 'BefittingLife Academy',
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      },
      {
        href: 'https://cdn.jsdelivr.net/npm/@mdi/font@latest/css/materialdesignicons.min.css',
        rel: 'stylesheet'
      }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: '~/components/Loading.vue',
  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/app.styl',
    '~/assets/style/css/override-fonts.css',
    'vue-plyr/dist/vue-plyr.css'
  ],
  env: env.parsed,
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/auth',
    '@/plugins/charts',
    { src: '~plugins/editor.js', ssr: false },
    { src: '~plugins/vuedraggable.js', ssr: false },
    '~plugins/vue-plyr.js'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    [
      'nuxt-stripe-module',
      {
        /* module options */
        version: 'v3', // Default
        publishableKey: 'pk_test_409gPDqwew8vUVLvvN7NvEGs'
      }
    ]
  ],
  devModules: [
    '@nuxtjs/vuetify',
    '@nuxtjs/eslint-module'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: 'https://befittinglife-academy-server.horlamedhey.now.sh',
    // baseURL: 'http://localhost/api',
    retry: { retries: 3 }
    // See https://github.com/nuxt-community/axios-module#options
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    theme: {
      primary: '#00a685',
      accent: colors.grey.darken3,
      secondary: '#345',
      info: colors.teal.lighten1,
      warning: colors.amber.base,
      error: colors.deepOrange.accent4,
      success: colors.green.accent3
    },
    treeShake: true,
    iconfont: 'mdi'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
