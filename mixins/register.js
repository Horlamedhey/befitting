const Cookie = process.client ? require('js-cookie') : undefined
export default {
  data() {
    return {
      loading: false,
      form: true,
      formType: 'login',
      passIcon: true,
      snackbar: {
        status: false,
        color: null,
        icon: null,
        content: null,
        addition: null,
        addAction: false,
        time: null,
        actiontype: 'resend'
      },
      fullname: '',
      email: '',
      password: '',
      checkPass: [v => !!v || 'Please input a password.'],
      checkName: [v => !!v || 'Please input your name.'],
      checkEmail: [
        v => !!v || 'Please input an email address.',
        v => /.+@.+/.test(v) || 'Please input a valid email address',
        v =>
          !/admin@befittinglife.com/.test(v) ||
          'Please login through the admin portal'
      ],
      checkPassFormat: [
        v => !!v || 'Please input a password.',
        v =>
          (/[A-Z]/.test(v) && /[a-z]/.test(v) && /[0-9]/.test(v)) ||
          'Password must contain UPPERCASE, lowercase, number and special-character'
      ]
    }
  },
  methods: {
    async register() {
      this.loading = true
      if (this.$refs.form.validate()) {
        const credentials = await {
          name: this.fullname,
          email: this.email.toLowerCase(),
          password: this.password,
          activationCode: this.generateRandomString()
        }
        await this.$axios
          .$post('/register', credentials)
          .then(res => {
            this.loading = false
            this.messageHandler(res)
          })
          .then(() => {
            this.formType = 'login'
          })
          .catch(err => {
            this.loading = false
            this.messageHandler(err.response.data)
          })
      } else {
        this.loading = false
      }
    },
    messageHandler(sender) {
      this.snackbar.content = sender.text
      this.snackbar.addition = sender.addition
      this.snackbar.addAction = sender.addAction
      this.snackbar.actiontype = sender.actiontype
      this.snackbar.color = sender.color
      this.snackbar.icon = sender.icon
      this.snackbar.status = true
      this.snackbar.time = sender.time
    },
    handleSnackbarAction() {
      if (this.snackbar.actiontype === 'resend') {
        this.reMail()
      } else if (this.snackbar.actiontype === 'logout') {
        this.snackbar.status = false
        Cookie.remove('auth')
        this.$store.commit('setUser', null)
      }
    },
    async reMail() {
      this.loading = true
      await this.$axios
        .$post('/sendMail', {
          email: this.email,
          activationCode: this.generateRandomString()
        })
        .then(() => {
          this.loading = false
          this.messageHandler({
            text: 'Mail sent successfully.',
            color: 'success',
            icon: 'mdi-account-check-outline',
            time: 0,
            addAction: true,
            actiontype: 'resend'
          })
        })
        .catch(() => {
          this.loading = false
          this.messageHandler({
            text: 'Unable to send mail at this time.',
            addition: 'Please try again.',
            color: 'warning',
            icon: 'mdi-alert-decagram-outline',
            time: 0,
            addAction: true,
            actiontype: 'resend'
          })
        })
    },
    generateRandomString() {
      const characters = process.env.secret
      let randomString = ''
      for (let i = 0; i <= 10; i++) {
        randomString +=
          characters[Math.floor(Math.random() * characters.length)]
      }
      return randomString
    }
  }
}
