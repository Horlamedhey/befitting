import axios from '~/conf'
const cookieparser = process.server ? require('cookieparser') : undefined
const Cookie = process.server ? require('js-cookie') : undefined
export const state = () => ({
  token: null,
  user: null,
  loggedIn: false,
  watched: [],
  toolbar: { flat: true },
  clientLocationDetails: {},
  feedbacks: []
})

export const mutations = {
  setToken(state, token) {
    state.token = token
  },
  setUser(state, user) {
    state.user = user
    state.loggedIn = user !== null
  },
  updateUser(state, userDetails) {
    state.user = { ...userDetails, ...state.user }
  },
  addWatched(state, payload) {
    state.watched = payload
  },
  updateWatched(state, payload) {
    state.watched.push(payload)
  },
  setLocationDetails(state, payload) {
    state.clientLocationDetails = payload
  },
  addfeedback(state, payload) {
    state.feedbacks.push(payload)
  },
  populateFeedbacks(state, payload) {
    state.feedbacks = payload
  }
}

export const actions = {
  async nuxtServerInit({ commit, state }, { req, redirect }) {
    let auth = null
    if (req.headers.cookie) {
      const parsed = cookieparser.parse(req.headers.cookie)
      try {
        auth = JSON.parse(parsed.auth)
      } catch (err) {
        console.log('No valid cookie found')
      }
      if (auth) {
        const { accessToken } = auth
        const config = {
          headers: {
            Authorization: `Bearer ${accessToken}`
          }
        }
        let response
        if (/profile/.test(req.url)) {
          response = await axios.get(`/user?refetch=true`, config)
          const { user, accessToken } = response.data
          const auth = { accessToken: accessToken }
          Cookie.set('auth', auth, { expires: 0.25 })
          commit('setUser', user)
          commit('setToken', accessToken)
        } else {
          response = await axios.get(`/user`, config)
          const user = response.data
          commit('setUser', user)
          commit('setToken', accessToken)
        }
      }
    }
    if (req.url === '/' || req.url === '/about') {
      const feedbacks = await axios.get('/fetchFeedbacks')
      commit('populateFeedbacks', feedbacks.data)
    }
    if (state.loggedIn && req.url === '/admin/dashboard') {
      if (state.user.email !== 'admin@befittinglife.com') {
        return redirect('/')
      }
    } else if (state.loggedIn && req.url === '/profile') {
      if (state.user.email === 'admin@befittinglife.com') {
        return redirect('/')
      }
    } else if (!state.loggedIn && req.url === '/admin/dashboard') {
      return redirect('/admin/login?logerr')
    } else if (
      !state.loggedIn &&
      (req.url === '/profile' ||
        req.url === '/profile?subscription=true' ||
        req.url === '/profile?subscription=false')
    ) {
      return redirect('/sign?logerr')
    }
  },
  async fetchFeedbacks({ commit }) {
    await axios.get('/fetchFeedbacks').then(res => {
      commit('populateFeedbacks', res.data)
    })
  }
}
