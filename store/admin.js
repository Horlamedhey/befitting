const Cookie = process.client ? require('js-cookie') : undefined

export const state = () => ({
  drawer: null,
  active: { name: 'Dashboard', component: 'Dashboard' },
  color: 'primary',
  mode: 'light',
  image: require('~/assets/admin1.jpg')
})

export const mutations = {
  async setColor(state, color) {
    state.color = color
    const theme = await Cookie.getJSON('theme')
    theme.color = color
    Cookie.set('theme', theme, { expires: 18250 })
  },
  async setImage(state, image) {
    state.image = image
    const theme = await Cookie.getJSON('theme')
    theme.image = image
    Cookie.set('theme', theme, { expires: 18250 })
  },
  async setMode(state, mode) {
    state.mode = mode
    const theme = await Cookie.getJSON('theme')
    theme.mode = mode
    Cookie.set('theme', theme, { expires: 18250 })
  },
  setActive(state, val) {
    state.active = { ...val }
  },
  setDrawer(state, val) {
    state.drawer = val
  }
}
